package com.gitlab.johnjvester.taskexecutor.runnables;

import com.gitlab.johnjvester.taskexecutor.models.Client;
import com.gitlab.johnjvester.taskexecutor.events.ServerSentEventsHandler;
import com.gitlab.johnjvester.taskexecutor.events.StopProcessingEventPublisher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SimpleTask implements Runnable {
    private String sseId;
    private ServerSentEventsHandler serverSentEventsHandler;
    private StopProcessingEventPublisher stopProcessingEventPublisher;

    @Override
    public void run() {
        log.info("running SampleTask() for sseId={}", sseId);
        Optional<Client> optional = serverSentEventsHandler.getClient(this.sseId);

        if (optional.isPresent()) {
            serverSentEventsHandler.broadcast(sseId, "Starting SimpleTask()");
        }

        try {
            for (int i = 0; i < 100; i++) {
                optional = serverSentEventsHandler.getClient(this.sseId);

                if (optional.isEmpty()) {
                    log.info("sseId={} does not exist", sseId);
                    stopProcessingEventPublisher.publishStopProcessingEvent(sseId);
                }

                log.info("sseId={}, i={}", sseId, i);
                serverSentEventsHandler.broadcast(sseId, "i=" + i);

                TimeUnit.SECONDS.sleep(2);
            }

            serverSentEventsHandler.broadcast(sseId, "Finished SimpleTask()");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        log.info("finished SampleTask() for sseId={}", sseId);
    }
}
