package com.gitlab.johnjvester.taskexecutor.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class DateUtils {
    private DateUtils() { }
    private static final DateTimeFormatter DEFAULT_DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("M/d/yyyy h:mm:ss a");
    public static String getDateTimeNow() {
        return LocalDateTime.now().format(DEFAULT_DATE_TIME_FORMAT);
    }
}
