package com.gitlab.johnjvester.taskexecutor.controllers;

import com.gitlab.johnjvester.taskexecutor.events.ServerSentEventsHandler;
import com.gitlab.johnjvester.taskexecutor.services.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RequiredArgsConstructor
@RestController
public class TaskController {
    private final ServerSentEventsHandler serverSentEventsHandler;
    private final TaskService taskService;

    @PostMapping(value = "/task/{thread_name}")
    public ResponseEntity<String> startTask(@PathVariable("thread_name") String threadName) {
        taskService.addTask(threadName);
        return new ResponseEntity<>("Started " + threadName, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/task/{thread_name}")
    public ResponseEntity<String> stopTask(@PathVariable("thread_name") String threadName) {
        taskService.stopTask(threadName);
        return new ResponseEntity<>("Stopped " + threadName, HttpStatus.CREATED);
    }

    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter sseEmitter(@RequestParam String sseId) {
        return serverSentEventsHandler.registerClient(sseId);
    }
}
