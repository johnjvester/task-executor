package com.gitlab.johnjvester.taskexecutor.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class StopProcessingEvent extends ApplicationEvent {
    private final String sseId;

    public StopProcessingEvent(Object source, String sseId) {
        super(source);
        this.sseId = sseId;
    }
}
