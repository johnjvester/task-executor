package com.gitlab.johnjvester.taskexecutor.events;

import com.gitlab.johnjvester.taskexecutor.services.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class StopProcessingEventListener implements ApplicationListener<StopProcessingEvent> {
    private final TaskService taskService;

    @Override
    public void onApplicationEvent(StopProcessingEvent event) {
        log.info("onApplicationEvent() event={}", event.getSseId());
        taskService.stopTask(event.getSseId());
    }
}
