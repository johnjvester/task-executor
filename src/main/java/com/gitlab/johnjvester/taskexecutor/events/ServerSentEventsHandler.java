package com.gitlab.johnjvester.taskexecutor.events;

import com.gitlab.johnjvester.taskexecutor.models.Client;
import com.gitlab.johnjvester.taskexecutor.models.StatusEvent;
import com.gitlab.johnjvester.taskexecutor.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Component
public class ServerSentEventsHandler {
    private static final AtomicInteger ID_COUNTER = new AtomicInteger(1);
    public static final long DEFAULT_TIMEOUT = 300_000L; // Five minutes, which should be long enough for this example
    private final Set<Client> registeredClients = new HashSet<>();

    public Optional<Client> getClient(String sseId) {
        return registeredClients.stream().filter(client -> client.getSseId().equals(sseId)).findFirst();
    }

    public SseEmitter registerClient(String sseId) {
        var emitter = new SseEmitter(DEFAULT_TIMEOUT);
        var client = new Client(emitter, sseId);
        emitter.onCompletion(() -> registeredClients.remove(client));
        emitter.onError(err -> removeAndLogError(client));
        emitter.onTimeout(() -> removeAndLogError(client));
        registeredClients.add(client);
        sendWelcomeMessageToClient(client);

        log.info("New client registered sseId={}", client.getSseId());
        return emitter;
    }

    private void removeAndLogError(Client client) {
        log.info("SSE communication error encountered. Unregistering client sseId={}", client.getSseId());
        registeredClients.remove(client);
    }

    private void sendWelcomeMessageToClient(Client client) {
        StatusEvent statusEvent = new StatusEvent("SSE listener connected. Welcome " + client.getSseId(), client.getSseId(), DateUtils.getDateTimeNow());
        sendMessage(client, statusEvent);
    }

    public void broadcast(String sseId, String message) {
        List<Client> clientList = List.copyOf(registeredClients);
        Optional<Client> optional = clientList.stream().filter(c -> c.getSseId().equals(sseId)).findFirst();
        optional.ifPresent(client -> sendMessage(client, new StatusEvent(message, sseId, DateUtils.getDateTimeNow())));
    }

    private void sendMessage(Client client, StatusEvent statusEvent) {
        var sseEmitter = client.getSseEmitter();
        try {
            log.debug("Notify client sseId={} statusEvent={}", client.getSseId(), statusEvent);

            var eventId = ID_COUNTER.incrementAndGet();

            SseEmitter.SseEventBuilder eventBuilder = SseEmitter.event()
                    .name(statusEvent.getEventType())
                    .id(String.valueOf(eventId))
                    .data(statusEvent, MediaType.APPLICATION_JSON);
            sseEmitter.send(eventBuilder);
        } catch (IOException e) {
            sseEmitter.completeWithError(e);
        }
    }
}
