package com.gitlab.johnjvester.taskexecutor.events;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class StopProcessingEventPublisher {
    private final ApplicationEventPublisher applicationEventPublisher;

    public void publishStopProcessingEvent(final String sseId) {
        log.info("publishStopProcessingEvent() sseId={}", sseId);
        StopProcessingEvent stopProcessingEvent = new StopProcessingEvent(this, sseId);
        applicationEventPublisher.publishEvent(stopProcessingEvent);
    }
}
