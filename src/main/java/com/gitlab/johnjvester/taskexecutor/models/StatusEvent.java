package com.gitlab.johnjvester.taskexecutor.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StatusEvent implements SseEvent {
    private String message;
    private String sseId;
    private String dateTimeString;

    @Override
    public String getEventType() {
        return "status";
    }
}
