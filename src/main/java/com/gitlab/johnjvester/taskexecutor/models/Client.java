                package com.gitlab.johnjvester.taskexecutor.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Client {
    private SseEmitter sseEmitter;
    private String sseId;
}
