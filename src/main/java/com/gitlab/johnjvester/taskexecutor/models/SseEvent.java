package com.gitlab.johnjvester.taskexecutor.models;

public interface SseEvent {
    String getEventType();
}
