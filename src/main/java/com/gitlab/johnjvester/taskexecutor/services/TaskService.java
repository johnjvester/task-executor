package com.gitlab.johnjvester.taskexecutor.services;

import com.gitlab.johnjvester.taskexecutor.events.ServerSentEventsHandler;
import com.gitlab.johnjvester.taskexecutor.events.StopProcessingEventPublisher;
import com.gitlab.johnjvester.taskexecutor.runnables.SimpleTask;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskService {
    private final ServerSentEventsHandler serverSentEventsHandler;
    private final StopProcessingEventPublisher stopProcessingEventPublisher;
    private final ConcurrentHashMap<String, Future> futureMap = new ConcurrentHashMap<>();
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @Async
    public void addTask(String taskName){
        log.info("addTask() taskName={}", taskName);
        futureMap.put(taskName, executorService.submit(new SimpleTask(taskName, serverSentEventsHandler, stopProcessingEventPublisher)));
    }

    public void stopTask(String taskName) {
        log.info("stopTask() taskName={}", taskName);
        Future taskNeedsToBeStop = futureMap.get(taskName);
        taskNeedsToBeStop.cancel(true);
    }
}

