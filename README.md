# `task-executor`

[![pipeline status](https://gitlab.com/johnjvester/task-executor/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/task-executor/commits/master)

> A simple example of using [server-sent events](https://en.wikipedia.org/wiki/Server-sent_events) (SSE) to trigger a 
> background task running on a [Spring Boot](https://en.wikipedia.org/wiki/Spring_Boot) RESTful API.

## About the `task-executor`

This repository was created in order to prototype the following use case:

> Allow consumers to initiate a SSE connection to a service, then start a long running task.  This task will run as a 
> background thread and send periodic updates to the consumer via the SSE connection.  When the long running task 
> completes, the thread will be available for the next consumer.  Multiple threads will be available, to allow more than 
> one consumer to be active at any given time.
> 
> The service should include a URI which will allow the consumer to stop their instance of the background task. For 
> example, a Stop button on an application could tell the service the background processing is no longer required.
> 
> As part of the background processing, the running task will check to see if the consumer still has an active SSE 
> connection.  If one no longer exists (the consumer disconnected their SSE connection or an unexpected error 
> occurred), the background task will stop running - saving resources on the `task-executor` service.

For this implementation, a [`SimpleTask`](src/main/java/com/gitlab/johnjvester/taskexecutor/runnables/SimpleTask.java) will be used. The `SimpleTask` will increment an `int` from `0` to `100`, 
pausing `2` seconds with each iteration. As part of each iteration, an SSE message will be broadcast to the listener, 
providing the current `int` value.  There are also broadcast messages when the `SimpleTask` starts and completes 
successfully.

As an FYI, the default timeout for the SSE client is `300_000` seconds, or five minutes.

## Getting Started

The `task-executor` repo is written in Spring Boot 3.x and has the following requirements:

* [Java](https://en.wikipedia.org/wiki/Java_(programming_language)) 17+ 
* [Gradle](https://en.wikipedia.org/wiki/Gradle) (wrapper already included)

To learn how to build and run a Spring Boot service, check out the following link:

https://spring.io/guides/gs/spring-boot/

## Using the `task-executor`

With the `task-executor` service running, use the following command to create a new SSE listener:

```shell
curl --location 'localhost:8500/stream?sseId=thread1'
```

With the SSE connection established, execute the following command to start a new instance of the `SimpleTask`:

```shell
curl --location --request POST 'localhost:8500/task/thread1'
```

If desired, the `thread` background instance of `SimpleTask` can be stopped using the following command:

```shell
curl --location --request DELETE 'localhost:8500/task/thread1'
```

To test what happens when the SSE listener stops, simply abort (Control-C) the SSE listener called by the `/stream` URI.  

## Questions & Contacts

Made with <span style="color:red;">♥</span> by [johnjvester](https://www.linkedin.com/in/johnjvester/), because 
I enjoy writing code.